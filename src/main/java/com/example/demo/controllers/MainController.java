package com.example.demo.controllers;

import com.example.demo.models.*;
import com.example.demo.repositories.GroupRepository;
import com.example.demo.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;


@Controller
public class MainController {
    @Autowired
    private final GroupService groupService;
    @Autowired
    private final TicketTestConnectionService ticketTestConnectionService;
    @Autowired
    private final StudentService studentService;
    @Autowired
    private final TeacherService teacherService;
    @Autowired
    private final GroupTeacherConnectionService groupTeacherConnectionService;
    @Autowired
    private final ModuleService moduleService;
    @Autowired
    private final QuestionService questionService;
    @Autowired
    private final TicketService ticketService;
    @Autowired
    private final TicketQuestionConnectionService ticketQuestionConnectionService;
    @Autowired
    private final TestService testService;
    @Autowired
    private final MarkService markService;
    @Autowired
    private final PersonService personService;

    public MainController(GroupRepository groupRepository, GroupService groupService, TicketTestConnectionService ticketTestConnectionService, StudentService studentService, TeacherService teacherService, GroupTeacherConnectionService groupTeacherConnectionService, ModuleService moduleService, QuestionService questionService, TicketService ticketService, TicketQuestionConnectionService ticketQuestionConnectionService, TestService testService, MarkService markService, PersonService personService) {
        this.groupService = groupService;
        this.ticketTestConnectionService = ticketTestConnectionService;
        this.studentService = studentService;
        this.teacherService = teacherService;
        this.groupTeacherConnectionService = groupTeacherConnectionService;
        this.moduleService = moduleService;
        this.questionService = questionService;
        this.ticketService = ticketService;
        this.ticketQuestionConnectionService = ticketQuestionConnectionService;
        this.testService = testService;
        this.markService = markService;
        this.personService = personService;
    }

    @GetMapping("/")
    public String home(Model model) {
        return "login";
    }

    @GetMapping("/login")
    public ResponseEntity<String> loginIn(@RequestParam(value = "login") String login,
                                          @RequestParam(value = "password") String password) {
        if (personService.thisPersonExists(login, password)) {
            Person person = personService.findByLogin(login);
            String token = personService.generateToken(login);
            String role = person.getRole();
            return new ResponseEntity<>(token + " " + role, HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Ошибка логина или пароля", HttpStatus.BAD_REQUEST);
        }
    }


    @GetMapping("/admin")
    public ModelAndView adminInterface(@RequestParam(value = "token") String token, Model model) {
        if (personService.findByToken(token).getRole().equals("admin")) {
            ModelAndView mav = new ModelAndView("admin");
            mav.addObject("people", personService.getAllTable());
            return mav;
        }
        return null;
    }

    @GetMapping("/adminAddUser")
    public ResponseEntity<String> addUser(@RequestParam(value = "login") String login,
                                          @RequestParam(value = "pass") String password,
                                          @RequestParam(value = "roleText") String role,
                                          @RequestParam(value = "token") String token) {
        if (personService.findByLogin(login) != null) {
            return new ResponseEntity<>("Логин занят", HttpStatus.BAD_REQUEST);
        }
        if (!personService.findByToken(token).getRole().equals("admin")) {
            return new ResponseEntity<>("ты не админ! Пошёл вон", HttpStatus.BAD_REQUEST);
        }
        personService.createPerson(login, password, role);
        //тлоько один ответ со статусом 200, потому что именно по стаутусу js помйет что все ок
        return new ResponseEntity<>("человечек создан", HttpStatus.OK);

    }

    @GetMapping("/adminAddGroup")
    public ResponseEntity<String> addGroup(@RequestParam(value = "name") String name,
                                           @RequestParam(value = "token") String token) {
        if (personService.findByToken(token).getRole().equals("admin")) {
            if (groupService.findByName(name) == null) {
                groupService.createGroup(name);
                return new ResponseEntity<>("ok", HttpStatus.OK);
            }
        }
        return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
    }

    @GetMapping("/adminAddStudentGroupConnection")
    public ResponseEntity<String> addGroupStudentConnection(@RequestParam(value = "student") String name,
                                                            @RequestParam(value = "group") String group,
                                                            @RequestParam(value = "token") String token) {
        Student s = studentService.findByName(name);
        Long id = s.getId();
        Long groupId = groupService.findByName(group).getId();
        if (personService.findByToken(token).getRole().equals("admin")) {
            if (studentService.findByIdAndGroupId(id, groupId) == null) {
                s.setGroupId(groupId);
                studentService.save(s);
                return new ResponseEntity<>("ok", HttpStatus.OK);
            }
        }
        return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
    }

    @GetMapping("adminAddGroupTeacherConnection")
    public ResponseEntity<String> addGroupTeacherConnection(@RequestParam(value = "teacherName") String teacherName,
                                                            @RequestParam(value = "groupName") String groupName,
                                                            @RequestParam(value = "token") String token) {
        if (!personService.findByToken(token).getRole().equals("admin")) {
            return new ResponseEntity<>("you shall not pass!", HttpStatus.BAD_REQUEST);
        }
        if (personService.thisPersonExists(teacherName) & groupService.thisGroupExists(groupName)) {
            groupTeacherConnectionService.addConnection(teacherService.findByPeopleId(personService.findByLogin(teacherName).getId()).getId(), groupService.findByName(groupName).getId());
            return new ResponseEntity<>("ok", HttpStatus.OK);
        }
        return new ResponseEntity<>("notOk", HttpStatus.BAD_REQUEST);
    }

    @GetMapping("/teacher")
    public ModelAndView teacherInterface(@RequestParam(value = "token") String token, Model model) {
        if (personService.findByToken(token).getRole().equals("teacher")) {
            Long id = teacherService.findByToken(token).getId();
            ModelAndView mav = new ModelAndView("teacher");
            System.out.println(teacherService.findAllByToken(token).size());
            mav.addObject("groups", teacherService.findAllByToken(token));
            mav.addObject("question", questionService.findAllByToken(token));
            mav.addObject("module", moduleService.findAllByTeacherId(id));
            mav.addObject("ticket", ticketService.findAllByTeacherId(id));
            List<GroupTeacherConnection> groupsConnections = groupTeacherConnectionService.findAllByTeacherId(teacherService.findByToken(token).getId());
            List<Group> groups = new ArrayList<Group>();
            for (int i = 0; i < groupsConnections.size(); i++) {
                groups.add(groupService.findById(groupsConnections.get(i).getGroupId()));
            }

            List<Student> students = new ArrayList<Student>();
            for (int i = 0; i < groups.size(); i++) {
                students.addAll(studentService.findAllByGroupId(groups.get(i).getId()));
            }
            List<Person> people = new ArrayList<Person>();
            for (int i = 0; i < students.size(); i++) {
                people.add(personService.findById(students.get(i).getPeopleId()));
            }
            mav.addObject("people", people);
            return mav;
        }
        return null;
    }

    @GetMapping("teacherAddModule")
    public ResponseEntity<String> addModule(@RequestParam(value = "moduleName") String moduleName,
                                            @RequestParam(value = "token") String token) {
        if (!personService.findByToken(token).getRole().equals("teacher")) {
            return new ResponseEntity<>("you shall not pass!", HttpStatus.BAD_REQUEST);
        }
        if (moduleService.thisModuleExist(moduleName)) {
            return new ResponseEntity<>("такой модуль уже существует", HttpStatus.BAD_REQUEST);
        } else {
            moduleService.createModule(moduleName, teacherService.findByToken(token).getId());
            return new ResponseEntity<>("модуль создан", HttpStatus.OK);
        }
    }
    @GetMapping("teacherAddTicketToModule")
    public ResponseEntity<String> addTicketToModule(@RequestParam(value = "namemodule") String moduleName,
                                                    @RequestParam(value = "nameTicket") String ticketName,
                                            @RequestParam(value = "token") String token) {
        if (!personService.findByToken(token).getRole().equals("teacher")) {
            return new ResponseEntity<>("you shall not pass!", HttpStatus.BAD_REQUEST);
        }
        if (moduleService.thisModuleExist(moduleName)) {
            if (!ticketTestConnectionService.thisTicketTestConnectionExist(moduleService.findModuleByName(moduleName).getId(),
                    ticketService.getByName(ticketName).getId())) {
                ticketTestConnectionService.addConnection(moduleService.findModuleByName(moduleName).getId(),
                        ticketService.getByName(ticketName).getId());
                return new ResponseEntity<>("модуль создан", HttpStatus.OK);
            }
            return new ResponseEntity<>("связь уже есть", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("такого модуля нет", HttpStatus.BAD_REQUEST);
    }

    @GetMapping("/teacherAddQuestion")
    public ResponseEntity<String> addQuestion(@RequestParam(value = "nameQuestion") String nameQuestion,
                                              @RequestParam(value = "nameAnswer") String nameAnswer,
                                              @RequestParam(value = "nameModule") String nameModule,
                                              @RequestParam(value = "token") String token) {
        if (!personService.findByToken(token).getRole().equals("teacher")) {
            return new ResponseEntity<>("you shall not pass!", HttpStatus.BAD_REQUEST);
        }
        if (questionService.thisQuestionExist(nameQuestion)) {
            return new ResponseEntity<>("такой вопрос уже есть", HttpStatus.BAD_REQUEST);
        } else {
            if (moduleService.moduleBelongsToTeacher(nameModule, token)) {
                //этот иф также учитывает и фозможность несуществования модуля
                questionService.createQuestion(moduleService.findModuleByName(nameModule).getId(), nameQuestion, nameAnswer);
                return new ResponseEntity<>("вопрос создан", HttpStatus.OK);
            }
            return new ResponseEntity<>("такого модуля нет", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/teacherAddTicket")
    public ResponseEntity<String> addTicker(@RequestParam(value = "nameTicket") String nameTicket,
                                            @RequestParam(value = "token") String token) {
        if (!personService.findByToken(token).getRole().equals("teacher")) {
            return new ResponseEntity<>("you shall not pass!", HttpStatus.BAD_REQUEST);
        }
        if (ticketService.thisTicketExist(nameTicket)) {
            return new ResponseEntity<>("такой билет уже существует", HttpStatus.BAD_REQUEST);
        } else {
            ticketService.createTicket(nameTicket, teacherService.findByToken(token).getId());
            return new ResponseEntity<>("билет создан", HttpStatus.OK);
        }
    }

    @GetMapping("/teacherAddTicketTestConnection")
    public ResponseEntity<String> addTicketTestConnection(@RequestParam(value = "nameTicket") String nameTicket,
                                                          @RequestParam(value = "nameTest") String nameTest,
                                                          @RequestParam(value = "token") String token) {
        if (!personService.findByToken(token).getRole().equals("teacher")) {
            return new ResponseEntity<>("you shall not pass!", HttpStatus.BAD_REQUEST);
        }

        if (!(ticketService.TicketBelongsToTeacher(nameTicket, token) && testService.TestExists(nameTest)))
            if (ticketTestConnectionService.thisTicketTestConnectionExist(ticketService.getByName(nameTicket).getId(),
                    testService.getByName(nameTest).getId())) {
                return new ResponseEntity<>("такая связь уже существует", HttpStatus.BAD_REQUEST);
            } else {
                ticketTestConnectionService.createTicketTestConnection(ticketService.getByName(nameTicket).getId(),
                        testService.getByName(nameTest).getId());
                return new ResponseEntity<>("связь создана", HttpStatus.OK);
            }
        return new ResponseEntity<>("error", HttpStatus.BAD_REQUEST);
    }

    @GetMapping("/teacherAddTicketQuestionConnection")
    public ResponseEntity<String> addTicketQuestionConnection(@RequestParam(value = "ticket") String nameTicket,
                                                              @RequestParam(value = "question") String textQuestion,
                                                              @RequestParam(value = "token") String token) {
        if (!personService.findByToken(token).getRole().equals("teacher")) {
            return new ResponseEntity<>("you shall not pass!", HttpStatus.BAD_REQUEST);
        }

        if (ticketQuestionConnectionService.thisTicketQuestionConnectionExist(ticketService.getByName(nameTicket).getId(),
                questionService.getByText(textQuestion).getId())) {
            return new ResponseEntity<>("такая связь уже существует", HttpStatus.BAD_REQUEST);
        } else {
            ticketQuestionConnectionService.createTicketQuestionConnection(ticketService.getByName(nameTicket).getId(),
                    questionService.getByText(textQuestion).getId());
            return new ResponseEntity<>("ok", HttpStatus.OK);
        }

    }

    @GetMapping("/teacherGenerateTicket")
    public ResponseEntity<String> generateTicket(@RequestParam(value = "ticket") String nameTicket,
                                                 @RequestParam(value = "numberOfQuestions") String numberOfQuestions,
                                                 @RequestParam(value = "numberOfModules") String numberOfModules,
                                                 @RequestParam(value = "token") String token) {
        if (!personService.findByToken(token).getRole().equals("teacher")) {
            return new ResponseEntity<>("you shall not pass!", HttpStatus.BAD_REQUEST);
        }
        if (ticketService.createRandomTicket(Integer.valueOf(numberOfModules), Integer.valueOf(numberOfQuestions), nameTicket, token)) {
            return new ResponseEntity<>("ok", HttpStatus.OK);
        }
        return new ResponseEntity<>("eror", HttpStatus.BAD_REQUEST);
    }


    @GetMapping("/teacherAddTest")
    public ResponseEntity<String> addTest(@RequestParam(value = "nameTest") String nameTest,
                                          @RequestParam(value = "nameGroup") String nameGroup,
                                          @RequestParam(value = "timeStart") Long timeStart,
                                          @RequestParam(value = "timeStop") Long timeStop,
                                          @RequestParam(value = "token") String token) {
        if (!personService.findByToken(token).getRole().equals("teacher")) {
            return new ResponseEntity<>("ты не пройдешь", HttpStatus.BAD_REQUEST);
        }
        Date tStart = new Date(timeStart);
        Date tStop = new Date(timeStop);
        testService.createTest(tStart, tStop, groupService.findByName(nameGroup).getId(), nameTest);
        return new ResponseEntity<>("тест создан", HttpStatus.OK);
    }

    @GetMapping("/teacherMarks")
    public ResponseEntity<String> teacherMarks(@RequestParam(value = "token") String token) {
        if (personService.findByToken(token).getRole().equals("teacher")) {
            String answer = "";
            List<GroupTeacherConnection> groupsC = groupTeacherConnectionService.findAllByTeacherId(teacherService.findByToken(token).getId());
           List<Group> groups=new ArrayList<Group>();
            for(int i=0;i<groupsC.size();i++){
                groups.add(groupService.findById(groupsC.get(i).getGroupId()));
            }
            for (int g = 0; g < groups.size(); g++) {
                answer += "{" + groups.get(g).getName() + "#ученик,";//название таблицы
                List<Test> tests = testService.findAllByGroupId(groups.get(g).getId());
                //заголовки
                for (int i = 0; i < tests.size(); i++) {
                    answer += tests.get(i).getName();
                    if (i != tests.size() - 1) {
                        answer += ",";
                    } else {
                        answer += "#";
                    }
                }
                //пошло тело таблицы
                List<Student> students = studentService.findAllByGroupId(groups.get(g).getId());
                Mark m;
                for (int i = 0; i < students.size(); i++) {
                    answer += personService.findById(students.get(i).getPeopleId()).getLogin() + ",";
                    for (int t = 0; t < tests.size(); t++) {
                        m=markService.getByStudentIdAndTestId(students.get(i).getId(), tests.get(t).getId());
                        if(m!=null) {
                            answer += String.valueOf(m.getMark());
                        }
                        else {
                            answer+="H";
                        }
                        if (t != tests.size() - 1) {
                            answer += ",";
                        }
                    }
                    answer += "#";
                }
                answer += "}";
            }
            return new ResponseEntity<>(answer, HttpStatus.OK);
        }
        return new ResponseEntity<>("you shall not pass!", HttpStatus.BAD_REQUEST);
    }


    @GetMapping("/student")
    public String studentInterface(@RequestParam(value = "token") String token, Model model) {
        if (personService.findByToken(token).getRole().equals("student")) {
            return "student";
        }
        return "login";
    }

    @GetMapping("/sendAnswers")
    public ResponseEntity<String> studentTesting(@RequestParam(value = "token") String token,
                                                 @RequestParam(value = "questions") String questions,
                                                 @RequestParam(value = "otvets") String answers,
                                                 @RequestParam(value = "testId")  Long testId) {
        if (!personService.findByToken(token).getRole().equals("student")) {
            return new ResponseEntity<>("you shall not pass!", HttpStatus.BAD_REQUEST);
        }


        String[] question = questions.split("#");
        String[] answer = answers.split("#");
        int mark = ticketService.checkTicket(answer, question);
        markService.addMark(testId,studentService.findByPeopleId(personService.findByToken(token).getId()).getId(),mark);
        return new ResponseEntity<>(String.valueOf(mark), HttpStatus.OK);
    }


    @GetMapping("/weakAdmin")
    public String weakAdminInterface(@RequestParam(value = "token") String token) {
        if (personService.findByToken(token).getRole().equals("weakAdmin")) {
            return "weakadmin";
        }
        return "login";
    }

    @GetMapping("/weakAdminGroups")
    public ResponseEntity<String> weakAdminGroups(@RequestParam(value = "token") String token) {
        if (personService.findByToken(token).getRole().equals("weakAdmin")) {
            String answer = "";
            List<Group> groups = groupService.getAll();
            for (int i = 0; i < groups.size(); i++) {
                answer += "{" + groups.get(i).getName() + "#ученик, учителя#";
                List<Student> students = studentService.findAllByGroupId(groups.get(i).getId());
                List<GroupTeacherConnection> teachers = groupTeacherConnectionService.findByGroupId(groups.get(i).getId());
                String teachStr = "";
                for (int t = 0; t < teachers.size(); t++) {
                    teachStr += personService.findById(teachers.get(t).getTeacherId()).getLogin() + "  ";
                }
                for (int s = 0; s < students.size(); s++) {
                    answer += personService.findById(students.get(s).getPeopleId()).getLogin() + "," + teachStr + "#";
                }
                answer += "}";
            }
            return new ResponseEntity<>(answer, HttpStatus.OK);
        }
        return new ResponseEntity<>("you shall not pass!", HttpStatus.BAD_REQUEST);
    }

    @GetMapping("/weakAdminMarks")
    public ResponseEntity<String> weakAdminTests(@RequestParam(value = "token") String token) {
        if (personService.findByToken(token).getRole().equals("weakAdmin")) {
            String answer = "";
            List<Group> groups = groupService.getAll();
            for (int g = 0; g < groups.size(); g++) {
                answer += "{" + groups.get(g).getName() + "#ученик,";//название таблицы
                List<Test> tests = testService.findAllByGroupId(groups.get(g).getId());
                //заголовки
                for (int i = 0; i < tests.size(); i++) {
                    answer += tests.get(i).getName();
                    if (i != tests.size() - 1) {
                        answer += ",";
                    } else {
                        answer += "#";
                    }
                }
                List<Student> students = studentService.findAllByGroupId(groups.get(g).getId());
                Mark m;
                for (int i = 0; i < students.size(); i++) {
                    answer += personService.findById(students.get(i).getPeopleId()).getLogin() + ",";
                    for (int t = 0; t < tests.size(); t++) {
                        m=markService.getByStudentIdAndTestId(students.get(i).getId(), tests.get(t).getId());
                        if(m!=null) {
                            answer += String.valueOf(m.getMark());
                        }
                        else {
                            answer+="H";
                        }
                        if (t != tests.size() - 1) {
                            answer += ",";
                        }
                    }
                    answer += "#";
                }
                answer += "}";
            }
            return new ResponseEntity<>(answer, HttpStatus.OK);
        }
        return new ResponseEntity<>("you shall not pass!", HttpStatus.BAD_REQUEST);
    }

    @GetMapping("/weakAdminTests")
    public ResponseEntity<String> weakAdminMarks(@RequestParam(value = "token") String token) {
        if (personService.findByToken(token).getRole().equals("weakAdmin")) {
            String answer = "{все тесты#название,время начала,время конца, название группы#";
            List<Test> tests = testService.getAll();
            for (int i = 0; i < tests.size(); i++) {
                answer += tests.get(i).getName() + "," + tests.get(i).getStartTime() + "," + tests.get(i).getEndTime() + "," +
                        groupService.findById(tests.get(i).getGroupId()).getName() + "#";
            }
            answer += "}";
            return new ResponseEntity<>(answer, HttpStatus.OK);
        }
        return new ResponseEntity<>("you shall not pass!", HttpStatus.BAD_REQUEST);
    }

    @GetMapping("/weakAdminTasks")
    public ResponseEntity<String> weakAdminTasks(@RequestParam(value = "token") String token) {
        if (personService.findByToken(token).getRole().equals("weakAdmin")) {
            String answer = "{все вопросы# вопрос,ответ,имя модуля#";
            List<Question> questions = questionService.getAll();
            for (int i = 0; i < questions.size(); i++) {
                answer += questions.get(i).getText() + "," + questions.get(i).getAnswer() + "," +
                        moduleService.findModuleById(questions.get(i).getModuleId()).getName() + "#";
            }
            answer += "}";
            return new ResponseEntity<>(answer, HttpStatus.OK);
        }
        return new ResponseEntity<>("you shall not pass!", HttpStatus.BAD_REQUEST);
    }

    @GetMapping("/studentInterface")
    public String studentInterface(@RequestParam(value = "token") String token) {
        if (personService.findByToken(token).getRole().equals("student")) {
            return "student";
        }
        return "login";
    }
    @GetMapping("/passTest")
    public String studentInterfacePassTest(@RequestParam(value = "token") String token,
                                           @RequestParam(value = "testId") Long testId) {
        if (personService.findByToken(token).getRole().equals("student")) {
            if (!(testService.getById(testId).getEndTime().after(new Date()) &
                    testService.getById(testId).getStartTime().before(new Date()))) {
                return "testsPassing";
            }
            return "testsPassing";
        }
        return "login";
    }

    @GetMapping("/studentTests")
    public ResponseEntity<String> studentTests(@RequestParam(value = "token") String token) {
        if (personService.findByToken(token).getRole().equals("student")) {
            String answer = "{тесты# название,дата начала, дата окончания, оценка#";
            List<Test> tests = testService.findAllByGroupId(studentService.findByPeopleId(personService.findByToken(token).getId()).getGroupId());
           Mark m;
            for (int i = 0; i < tests.size(); i++) {
                String link = "\\" + "passTest" + "?token=" + token + "&testId=" + tests.get(i).getId();
                answer += "<a href=" + '"' + link + '"' + ">" + tests.get(i).getName() + "</a>" + "," + tests.get(i).getStartTime() + "," + tests.get(i).getEndTime() + "," ;
                     m=markService.getByStudentIdAndTestId(studentService.findByPeopleId(personService.findByToken(token).getId()).getId(), tests.get(i).getId());
                     if(m==null){
                         answer+="Н";
                     }
                     else {
                         answer+=m.getMark();
                     }
                     answer+="#";
            }
            answer += "}";
            return new ResponseEntity<>(answer, HttpStatus.OK);
        }
        return new ResponseEntity<>("you shall not pass!", HttpStatus.BAD_REQUEST);
    }

    @GetMapping("/getTest")
    public ResponseEntity<String> getTest(@RequestParam(value = "token") String token, @RequestParam(value = "testId") Long testId) {
        if (personService.findByToken(token).getRole().equals("student")) {
            String answer = "";
            List<TicketTestConnection> ticketsConnections = ticketTestConnectionService.findAllByTestId(testId);
            if(ticketsConnections.size()==0){
               return new ResponseEntity<>("пустой тест", HttpStatus.OK);
            }
            Long chosenTicketId = ticketsConnections.get((int) random(0, ticketsConnections.size())).getTicketId();
            List<TicketQuestionConnection> questions = ticketQuestionConnectionService.findAllQuestionsInTicket(chosenTicketId);
            for(int i=0;i<questions.size();i++){
                answer+=questionService.getById(questions.get(i).getQuestionId()).getText();
                if(i!=questions.size()-1){
                    answer+="#";
                }
            }
            System.out.println(answer);
            return new ResponseEntity<>(answer, HttpStatus.OK);
        }
        return new ResponseEntity<>("you shall not pass!", HttpStatus.BAD_REQUEST);
    }

    private float random(float a, float b) {
        Random random = new Random();
        a *= 100;
        b *= 100;
        if (a > b) {
            float c = b;
            b = a;
            a = c;
        }
        float dif = b - a;
        return (random.nextInt((int) (dif + 1)) + a) / 100.0f;
    }
}