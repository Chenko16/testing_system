package com.example.demo.models;

import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;

@Entity
@Table(name = "class")
public class Group {
    @Column(name = "id")
    private Long id;
    @Column(name = "name")
    private String name;
    public Group(String name){
        setName(name);
    }

    public Group() {
    }
    public void setId(Long id) {
        this.id = id;
    }
    @Id
    @GenericGenerator(name="seq" , strategy="increment")
    @GeneratedValue(generator="seq")
    @Column(name="id")
    public Long getId() {
        return id;
    }
   public void setName(String name){
        this.name=name;
   }
   public String getName(){
        return this.name;
   }
}