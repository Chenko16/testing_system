package com.example.demo.models;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;


@Entity
@Table(name = "ticket")
public class Ticket {
    @Column(name = "id")
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "teacher_id")
    private Long teacherId;


    public Ticket() {
    }

    public Ticket(String name) {
        setName(name);
    }

    public Ticket(String name, Long teacherId) {
        setName(name);
        setTeacherId(teacherId);
    }


    public void setId(Long id) {
        this.id = id;
    }

    @Id
    @GenericGenerator(name = "seq", strategy = "increment")
    @GeneratedValue(generator = "seq")
    @Column(name = "id")
    public Long getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setTeacherId(Long teacherId) {
        this.teacherId = teacherId;
    }

    public Long getTeacherId() {
        return this.teacherId;
    }
}


