package com.example.demo.models;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "people")
public class Person {
    @Column(name = "login")
    private String login;
    @Column(name = "id")
    private Long id;
    @Column(name = "pass")
    private String pass;
    @Column(name = "role")
    private String role;
    @Column(name = "token")
    private String token;

    public Person(String login,String pass,String role){
        setPass(pass);
        setLogin(login);
        setRole(role);
    }

    public Person() {
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Id
    @GenericGenerator(name="seq" , strategy="increment")
    @GeneratedValue(generator="seq")
    @Column(name="id")
    public Long getId() {
        return id;
    }
    public String getPass(){
        return pass;
    }
    public void setPass(String pass){
        this.pass=pass;
    }
    public String getLogin(){
        return login;
    }
    public void setLogin(String login){
        this.login=login;
    }
    public String getRole(){
        return role;
    }
    public void setRole(String role){
        this.role=role;
    }
    public void setToken(String tok){
        this.token=tok;
    }
    public String getToken(){
        return this.token;
    }
}