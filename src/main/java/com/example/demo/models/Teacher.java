package com.example.demo.models;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "teacher")
public class Teacher {
    @Column(name = "id")
    private Long id;
    @Column(name = "people_id")
    private Long peopleId;

    public Teacher(Long peopleId) {
        setPeopleId(peopleId);
    }

    public Teacher() {
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Id
    @GenericGenerator(name = "seq", strategy = "increment")
    @GeneratedValue(generator = "seq")
    @Column(name = "id")
    public Long getId() {
        return id;
    }

    public void setPeopleId(Long peopleId) {
        this.peopleId = peopleId;
    }

    public Long getPeopleId() {
        return this.peopleId;
    }
}