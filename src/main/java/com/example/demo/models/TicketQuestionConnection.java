package com.example.demo.models;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "ticket_question")
public class TicketQuestionConnection {
    @Column(name = "question_id")
    private Long questionId;
    @Column(name = "ticket_id")
    private Long ticketId;
    @Column(name = "id")
    private Long id;

    public TicketQuestionConnection(Long ticketId, Long questionId) {
        setQuestionId(questionId);
        setTicketId(ticketId);
    }

    public TicketQuestionConnection() {
    }

    @Column(name = "ticket_id")
    public void setTicketId(Long ticketId) {
        this.ticketId = ticketId;
    }

    @Column(name = "ticket_id")
    public Long getTicketId() {
        return this.ticketId;
    }

    @Column(name = "question_id")
    public void setQuestionId(Long questionId) {
        this.questionId = questionId;
    }

    @Column(name = "question_id")
    public Long getQuestionId() {
        return this.questionId;
    }


    public void setId(Long id) {
        this.id = id;
    }

    @Id
    @GenericGenerator(name = "seq", strategy = "increment")
    @GeneratedValue(generator = "seq")
    @Column(name = "id")
    public Long getId() {
        return id;
    }
}
