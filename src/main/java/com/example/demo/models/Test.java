package com.example.demo.models;

import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "test")
public class Test {
    @Column(name = "id")
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "start_time")
    private Date startTime;
    @Column(name = "end_time")
    private Date endTime;
    @Column(name = "class_id")
    private Long groupId;

    public Test() {

    }

    public Test(Date startTime, Date endTime, Long groupId, String name) {
        setEndTime(endTime);
        setStartTime(startTime);
        setGroupId(groupId);
        setName(name);
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Id
    @GenericGenerator(name = "seq", strategy = "increment")
    @GeneratedValue(generator = "seq")
    @Column(name = "id")
    public Long getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    @Column(name = "start_time")
    public void setStartTime(Date d) {
        this.startTime = d;
    }

    @Column(name = "start_time")
    public Date getStartTime() {
        return this.startTime;
    }

    @Column(name = "end_time")
    public void setEndTime(Date d) {
        this.endTime = d;
    }

    @Column(name = "end_time")
    public Date getEndTime() {
        return this.endTime;
    }

    @Column(name = "class_id")
    public void setGroupId(Long i) {
        this.groupId = i;
    }

    @Column(name = "class_id")
    public Long getGroupId() {
        return this.groupId;
    }

}
