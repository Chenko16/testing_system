package com.example.demo.models;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "class_teacher")
public class GroupTeacherConnection {
    @Column(name = "teacher_id")
    private Long teacherId;
    @Column(name = "class_id")
    private Long groupId;
    @Column(name = "id")
    private Long id;

    public GroupTeacherConnection(Long teacherId, Long classId) {
        setGroupId(classId);
        setTeacherId(teacherId);
    }

    public GroupTeacherConnection() {
    }

    @Column(name = "class_id")
    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    @Column(name = "class_id")
    public Long getGroupId() {
        return groupId;
    }

    @Column(name = "teacher_id")
    public void setTeacherId(Long teacherId) {
        this.teacherId = teacherId;
    }

    @Column(name = "teacher_id")
    public Long getTeacherId() {
        return this.teacherId;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Id
    @GenericGenerator(name = "seq", strategy = "increment")
    @GeneratedValue(generator = "seq")
    @Column(name = "id")
    public Long getId() {
        return id;
    }
}