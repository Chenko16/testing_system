package com.example.demo.models;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "student")
public class Student {
    @Column(name = "id")
    private Long id;
    @Column(name = "class_id")
    private Long groupId;
    @Column(name = "people_id")
    private Long peopleId;

    public Student(Long groupId, Long peopleId) {
        setGroupId(groupId);
        setPeopleId(peopleId);
    }

    public Student() {
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Id
    @GenericGenerator(name = "seq", strategy = "increment")
    @GeneratedValue(generator = "seq")
    @Column(name = "id")
    public Long getId() {
        return id;
    }

    @Column(name = "class_id")
    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    @Column(name = "class_id")
    public Long getGroupId() {
        return this.groupId;
    }

    public void setPeopleId(Long peopleId) {
        this.peopleId = peopleId;
    }

    public Long getPeopleId() {
        return this.peopleId;
    }
}