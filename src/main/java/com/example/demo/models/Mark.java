package com.example.demo.models;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;


@Entity
@Table(name = "mark")
public class Mark {
    @Column(name = "id")
    private Long id;
    @Column(name = "test_id")
    private Long testId;
    @Column(name = "student_id")
    private Long studentId;
    @Column(name = "mark")
    private int mark;


    public Mark() {
    }

    public Mark(Long moduleId, String name) {

    }

    public Mark(Long testId, Long studentId, int mark) {
        setMark(mark);
        setStudentId(studentId);
        setTestId(testId);
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Id
    @GenericGenerator(name = "seq", strategy = "increment")
    @GeneratedValue(generator = "seq")
    @Column(name = "id")
    public Long getId() {
        return id;
    }

    public void setMark(int mark) {
        this.mark = mark;
    }

    public int getMark() {
        return this.mark;
    }

    public void setTestId(Long testId) {
        this.testId = testId;
    }

    public Long getTestId() {
        return this.testId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Long getStudentId() {
        return studentId;
    }
}

