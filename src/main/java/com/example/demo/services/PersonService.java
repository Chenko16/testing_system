package com.example.demo.services;

import com.example.demo.models.Person;
import com.example.demo.models.Student;
import com.example.demo.repositories.PeopleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class PersonService {
    @Autowired
    private final PeopleRepository peopleRepository;

    @Autowired
    GroupService groupService;
    @Autowired
    StudentService studentService;
    @Autowired
    TeacherService teacherService;
    @Autowired
    GroupTeacherConnectionService groupTeacherConnectionService;


    @Autowired
    public PersonService(PeopleRepository peopleRepository, EntityManager entityManager) {
        this.peopleRepository = peopleRepository;
        //  this.entityManager = entityManager;
    }

    public Person findById(long id) {
        return peopleRepository.findOneById(id);
    }

    public void save(Person person) {
        peopleRepository.save(person);
    }

    public void setPass(Person person, String pass) {
        person.setPass(pass);
        peopleRepository.save(person);
    }

    public void createPerson(String login, String pass, String role) {
        Person p = new Person(login, pass, role);
        peopleRepository.save(p);
        if (role.equals("student")) {
            studentService.addStudent(p.getId());
        }
        if (role.equals("teacher")) {
            teacherService.addTeacher(p.getId());
        }
    }

    public Person findByLogin(String login) {
        return peopleRepository.findByLogin(login);
    }

    public boolean thisPersonExists(String login, String pass) {
        Person p = peopleRepository.findByLogin(login);
        if (p != null) {
            if (p.getPass().equals(pass)) {
                return true;
            }
        }
        return false;
    }

    public boolean thisPersonExists(String login) {
        Person p = peopleRepository.findByLogin(login);
        if (p != null) {
            return true;
        }
        return false;
    }

    public Person findByToken(String token) {
        return peopleRepository.findByToken(token);
    }


    public boolean thisPersonExistsByToken(String token) {
        Person p = peopleRepository.findByToken(token);
        if (p != null) {
            return true;
        }
        return false;
    }


    public String generateToken(String login) {
        String token = getAlphaNumericString(20);
        while(peopleRepository.findByToken(token)!=null) {
            token = getAlphaNumericString(20);
        }

        Person per = peopleRepository.findByLogin(login);
        per.setToken(token);
        peopleRepository.save(per);
        return token;
    }


    static String getAlphaNumericString(int n) {
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" + "0123456789" + "abcdefghijklmnopqrstuvxyz";
        StringBuilder sb = new StringBuilder(n);
        for (int i = 0; i < n; i++) {
            int index = (int) (AlphaNumericString.length() * Math.random());
            sb.append(AlphaNumericString.charAt(index));
        }
        return sb.toString();
    }

    public List<Person>getAllTable(){
        return peopleRepository.getAll();
    }

}
