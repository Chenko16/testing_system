package com.example.demo.services;

import com.example.demo.models.Group;
import com.example.demo.models.Person;
import com.example.demo.models.Question;
import com.example.demo.repositories.GroupRepository;
import com.example.demo.repositories.PeopleRepository;
import com.example.demo.repositories.QuestionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import com.example.demo.models.Module;

@Service
public class QuestionService {
    @Autowired
    private final QuestionRepository questionRepository;

    @Autowired
    ModuleService moduleService;
    @Autowired
    TeacherService teacherService;

    @Autowired
    public QuestionService(QuestionRepository questionRepository, EntityManager entityManager) {
        this.questionRepository = questionRepository;
    }

    public Question getById(Long id) {
        return questionRepository.getById(id);
    }

    public List<Question> getAllByModuleId(Long id) {
        return questionRepository.getAllByModuleId(id);
    }

    public List<Question> getAllByAnswer(String answer) {
        return questionRepository.getAllByAnswer(formatAnswer(answer));
    }

    public Question getByText(String text) {
        return questionRepository.getByText(text);
    }

    public void createQuestion(Long moduleId, String text, String answer) {
        Question q = new Question(moduleId, text, formatAnswer(answer));
        questionRepository.save(q);
    }

    public void createQuestion(String text, String answer) {
        Question q = new Question(text, formatAnswer(answer));
        questionRepository.save(q);
    }

    public boolean thisQuestionExist(String name) {
        if (getByText(name) == null) {
            return false;
        } else {
            return true;
        }
    }

    public boolean answeredCorrectly(String questionText, String enteredAnswer) {
        enteredAnswer = formatAnswer(enteredAnswer);
        Question q = getByText(questionText);
        return q.getAnswer().equals(enteredAnswer);
    }

    public String formatAnswer(String answ) {
        answ = answ.replace(" ", "");
        answ = answ.toLowerCase();
        return answ;
    }

    public List<Question> findAllByToken(String teacherToken) {
        Long tId = teacherService.findByToken(teacherToken).getId();
        List<Question> questions = new ArrayList<Question>();
        List<Module> modules = moduleService.findAllByTeacherId(tId);
        for (int i = 0; i < modules.size(); i++) {
            questions.addAll(getAllByModuleId(modules.get(i).getId()));
        }
        return questions;
    }

    public List<Question> getAll() {
        return questionRepository.getAll();
    }
}
