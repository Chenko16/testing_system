package com.example.demo.services;

import com.example.demo.models.*;
import com.example.demo.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class TicketQuestionConnectionService {
    @Autowired
    private final TicketQuestionConnectionRepository ticketQuestionConnectionRepository;

    @Autowired
    public TicketQuestionConnectionService(TicketQuestionConnectionRepository ticketQuestionConnectionRepository, EntityManager entityManager) {
        this.ticketQuestionConnectionRepository = ticketQuestionConnectionRepository;
    }

    public List<TicketQuestionConnection> findAllTicketsWithQuestion(Long questionId) {
        return ticketQuestionConnectionRepository.findAllByQuestionId(questionId);
    }

    public List<TicketQuestionConnection> findAllQuestionsInTicket(Long ticketId) {
        return ticketQuestionConnectionRepository.findAllByTicketId(ticketId);
    }

    public void addConnection(Long ticketId, Long questionId) {
        if(ticketQuestionConnectionRepository.findByTicketIdAndQuestionId(ticketId,questionId)==null) {
            TicketQuestionConnection t = new TicketQuestionConnection(ticketId, questionId);
            ticketQuestionConnectionRepository.save(t);
            return;
        }
        System.out.println("in ticket-question-connection service ERORR: connection ticket "+
                String.valueOf(ticketId)+" to question "+String.valueOf(questionId)+" already exists");

    }

    public void createTicketQuestionConnection(long ticketId, long questionId){
        ticketQuestionConnectionRepository.save(new TicketQuestionConnection(ticketId, questionId));
    }

    public boolean thisTicketQuestionConnectionExist(long ticketId, long questionId){
        return !(ticketQuestionConnectionRepository.findByTicketIdAndQuestionId(ticketId, questionId)==null);
    }
}
