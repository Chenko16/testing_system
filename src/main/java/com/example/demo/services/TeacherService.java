package com.example.demo.services;

import com.example.demo.models.*;
import com.example.demo.repositories.GroupRepository;
import com.example.demo.repositories.PeopleRepository;
import com.example.demo.repositories.StudentRepository;
import com.example.demo.repositories.TeacherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class TeacherService {
    @Autowired
    private final TeacherRepository teacherRepository;


    @Autowired
    GroupTeacherConnectionService groupTeacherConnectionService;
    @Autowired
    StudentService studentService;
    @Autowired
    PersonService personService;
    @Autowired
    GroupService groupService;


    @Autowired
    public TeacherService(TeacherRepository teacherRepository, EntityManager entityManager) {
        this.teacherRepository = teacherRepository;
    }

    public Teacher findById(long id) {
        return teacherRepository.getById(id);
    }

    public void addTeacher(Long peopleId){
        Teacher t=new Teacher(peopleId);
        teacherRepository.save(t);
    }

    public List<Student> allStudentsOfTeacherWithId(Long id) {
        List students = new ArrayList();
        List<GroupTeacherConnection> groups = new ArrayList<GroupTeacherConnection>();
        groups = groupTeacherConnectionService.findAllByTeacherId(id);
        for (int i = 0; i < groups.toArray().length; i++) {
            GroupTeacherConnection g = groups.get(i);
            students.addAll(studentService.findAllByGroupId(g.getGroupId()));
        }
        return students;
    }
    public Teacher findByPeopleId(Long id){
        return  teacherRepository.getByPeopleId(id);
    }

    public List<Group> findAllByToken(String token){
        List<GroupTeacherConnection> groups;
        List <Group> group= new ArrayList();
        groups = groupTeacherConnectionService.findAllByTeacherId((findByPeopleId(personService.findByToken(token).getId()).getId()));
        for (int i = 0; i < groups.toArray().length; i++) {
            GroupTeacherConnection g = groups.get(i);
            group.add(groupService.findById(g.getGroupId()));
        }
        return group;
    }
    public Teacher findByToken(String token){
        Person p=personService.findByToken(token);
        return findByPeopleId(p.getId());
    }
}
