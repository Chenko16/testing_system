package com.example.demo.services;

import com.example.demo.models.Question;
import com.example.demo.models.Ticket;
import com.example.demo.repositories.TicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Random;

@Service
public class TicketService {
    @Autowired
    private final TicketRepository ticketRepository;

    @Autowired
    public TicketService(TicketRepository ticketRepository, EntityManager entityManager) {
        this.ticketRepository = ticketRepository;
    }

    @Autowired
    private TicketQuestionConnectionService ticketQuestionConnectionService;
    @Autowired
    private QuestionService questionService;
    @Autowired
    private ModuleService moduleService;
    @Autowired
    private TeacherService teacherService;
    @Autowired
    private PersonService personService;

    public Ticket getById(Long id) {
        return ticketRepository.getById(id);
    }

    public Ticket getByName(String name) {
        return ticketRepository.getByName(name);
    }

    public Ticket findByName(String name) {
        return ticketRepository.getByName(name);
    }

    public void createTicket(String name, Long teacherId) {
        //создает пустой билет, вопросы в него добавить надо через ticketQuestionConnection.
        if (findByName(name) == null) {
            Ticket t = new Ticket(name, teacherId);
            ticketRepository.save(t);
        } else {
            System.out.println("eror, ticket " + name + " already exists");
        }
    }

    public int checkTicket(String[] answers, String[] questions) {
        float totalScore = 0;
        if (answers.length != questions.length) {
            return -1;
        }
        for (int i = 0; i < answers.length; i++) {
            if (questionService.answeredCorrectly(questions[i], answers[i])) {
                totalScore++;
            }
        }
        float mark = totalScore / answers.length;
        if (mark > 0.8) {
            return 5;
        }
        if (mark > 0.65f) {
            return 4;
        }
        if (mark > 0.5f) {
            return 3;
        }
        if (mark > 0.45f) {
            return 2;
        }
        return 1;
    }

    public boolean thisTicketExist(String name) {
        if (getByName(name) != null) {
            return true;
        } else {
            return false;
        }
    }

    public boolean createRandomTicket(int numberOfModules, int numberOfQuestionsFromModule,
                                      String name, String teacherToken) {
        Ticket ticket;
        createTicket(name, teacherService.findByToken(teacherToken).getId());
        ticket = getByName(name);

        int modules[] = new int[numberOfModules];
        int totalQuestionsInModule;
        int totalModules = moduleService.findAllByTeacherId(teacherService.findByPeopleId(personService.findByToken(teacherToken).getId()).getId()).size();
        if (totalModules < numberOfModules) {
            return false;
        }

        for (int i = 0; i < modules.length; i++) {
            int r = 0;
            do {
                r = (int) random(1, totalModules + 1);
                modules[i] = r;
            } while (findInArry(r, modules, i));
        }
        Long moduleId;
        for (int i = 0; i < modules.length; i++) {
            int questions[] = new int[numberOfQuestionsFromModule];
            moduleId = Long.valueOf(modules[i]);
            //получаем все вопросы модуля
            List<Question> allQuestionsFromCurrentModule = questionService.getAllByModuleId(moduleId);
            totalQuestionsInModule = allQuestionsFromCurrentModule.size();
            if (numberOfQuestionsFromModule > totalQuestionsInModule) {
                System.out.println(totalQuestionsInModule);
                return false;
            }
            for (int g = 0; g < questions.length; g++) {
                int r = 0;
                do {
                    r = (int) random(1, totalQuestionsInModule);
                    questions[g] = r;
                } while (findInArry(r, questions, g));
            }
            for (int g = 0; g < questions.length; g++) {
                ticketQuestionConnectionService.addConnection(ticket.getId(), allQuestionsFromCurrentModule.get(questions[g] - 1).getId());
            }
        }
        return true;
    }

    private boolean findInArry(int n, int[] arr, int excludePos) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == n && i != excludePos) {
                return true;
            }
        }
        return false;
    }

    private float random(float a, float b) {
        Random random = new Random();
        a *= 100;
        b *= 100;
        if (a > b) {
            float c = b;
            b = a;
            a = c;
        }
        float dif = b - a;
        return (random.nextInt((int) (dif + 1)) + a) / 100.0f;
    }

    public boolean TicketBelongsToTeacher(String ticketName, String teacherToken) {
        List<Ticket> tickets = ticketRepository.findAllByTeacherId(teacherService.findByToken(teacherToken).getId());
        for (int i = 0; i < tickets.size(); i++) {
            if (tickets.get(i).getName().equals(ticketName)) {
                return true;
            }
        }
        return false;
    }

    public List<Ticket> findAllByTeacherId(Long id) {
        return ticketRepository.findByTeacherId(id);
    }

}
