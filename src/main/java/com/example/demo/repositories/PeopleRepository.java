package com.example.demo.repositories;

import com.example.demo.models.Person;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface PeopleRepository extends JpaRepository<Person, Long> {
    Person findOneById(Long id);

    //выполнит кастомный запрос переди выполнением функции под ним
    @Query("SELECT p FROM Person p where login = :login")
    Person findByLogin(@Param("login") String login);

    @Query("SELECT p FROM Person p where token = :token")
    Person findByToken(@Param("token") String login);

    @Modifying
    @Query("update Person p set token = :token where login = :login")
    void setTokenByLogin(@Param("token")  String token, @Param("login")  String login);

    //the same as SELECT * FROM people
    @Query("SELECT p FROM Person p ORDER BY login")
    List<Person> getAll();
}
