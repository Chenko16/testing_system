package com.example.demo.repositories;

import com.example.demo.models.Group;
import com.example.demo.models.Student;
import com.example.demo.models.Test;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface TestRepository extends JpaRepository<Test, Long> {
    Test getById(Long aLong);

    Test getByName(String name);

    List<Test> findAllByGroupId(Long id);

    Test findByName(String name);

    @Query("SELECT p FROM Test p")
    List<Test> getAll();
}