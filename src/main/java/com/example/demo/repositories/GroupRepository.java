package com.example.demo.repositories;

import com.example.demo.models.Group;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface GroupRepository extends JpaRepository<Group, Long> {
    Group getById(Long aLong);

    Group findByName(String name);

    @Query("SELECT p FROM Group p")
    List<Group> getAll();
}