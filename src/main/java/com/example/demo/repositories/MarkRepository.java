package com.example.demo.repositories;

import com.example.demo.models.Mark;
import com.example.demo.models.Ticket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface MarkRepository extends JpaRepository<Mark, Long> {
    Mark getById(Long id);

    List<Mark> getAllByStudentId(Long id);

    List<Mark> getAllByTestId(Long id);

    Mark getByStudentIdAndTestId(Long studentId,Long testId);
}