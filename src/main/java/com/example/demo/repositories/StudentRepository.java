package com.example.demo.repositories;

import com.example.demo.models.Group;
import com.example.demo.models.Person;

import com.example.demo.models.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface StudentRepository extends JpaRepository<Student, Long> {
    Student getById(Long id);

    List <Student> findAllByGroupId(Long id);

    Student findByIdAndAndGroupId(Long id,Long groupId);

    Student findByPeopleId(Long id);

    @Query("SELECT p FROM Student p")
    List<Student> getAll();
}