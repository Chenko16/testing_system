package com.example.demo.repositories;

import com.example.demo.models.Question;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface QuestionRepository extends JpaRepository<Question, Long> {
    Question getById(Long aLong);

    List<Question> getAllByModuleId(Long id);

    List<Question> getAllByAnswer(String answer);

    Question getByText(String text);

    @Query("SELECT p FROM Question p")
    List <Question> getAll();
}