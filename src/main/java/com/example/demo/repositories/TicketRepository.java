package com.example.demo.repositories;

import com.example.demo.models.Ticket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface TicketRepository extends JpaRepository<Ticket, Long> {
    Ticket getById(Long id);

    Ticket getByName(String name);

    List<Ticket> findAllByTeacherId(Long id);

    List<Ticket> findByTeacherId(Long id);
}