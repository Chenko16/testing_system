package com.example.demo.repositories;

import com.example.demo.models.Question;
import com.example.demo.models.Ticket;
import com.example.demo.models.TicketQuestionConnection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface TicketQuestionConnectionRepository extends JpaRepository<TicketQuestionConnection, Long> {
    List<TicketQuestionConnection> findAllByTicketId(Long id);

    List<TicketQuestionConnection> findAllByQuestionId(Long id);

    TicketQuestionConnection findByTicketIdAndQuestionId(Long ticketId,Long questionId);

}