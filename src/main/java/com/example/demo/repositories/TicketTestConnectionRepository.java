package com.example.demo.repositories;


import com.example.demo.models.TicketTestConnection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface TicketTestConnectionRepository extends JpaRepository<TicketTestConnection, Long> {
    List<TicketTestConnection> findAllByTicketId(Long ticketId);

    List<TicketTestConnection> findAllByTestId(Long testId);

    TicketTestConnection findByTicketIdAndTestId(Long ticketId, Long testId);
}