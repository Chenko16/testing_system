async function logIn() {

    let login = document.getElementById('login').value
    let password = document.getElementById('password').value
    // let request = await fetch(url, login, password);
    let x = new XMLHttpRequest();
    x.open("GET", "/login?login=" + login + "&password=" + password, true);
    x.send(null);
    x.onload = function () {
        localStorage.removeItem("logToken");
        if(x.status!=200){
            alert("wrong password or login")
        }
        else{
            let data=x.responseText.split(" ");
            localStorage.setItem("logToken",data[0]);
            window.location.href=window.location.href+data[1]+"?token="+data[0];
        }
    }
}